import '../../App/App.css';
import { useState } from 'react';
import apiService from '../../actions/api';

function FormAndResult() {
  const [input, setInput] = useState('');
  const [result, setResult] = useState({});
  const [error, setError] = useState(null);

  const handleSubmit = (e) => {
   e.preventDefault();

   let numbers = input.split(',');

   if (input.length === 0) {
     setResult('');
     return;
   }

   if (numbers.length === 1) {
      apiService.checkprime(numbers[0])
      .then(response => {
        console.log(response);
        setResult(response);
      })
      .catch(error => {
        console.log(error);
      });
   } else {
      apiService.checksumandprime(numbers)
      .then(response => {
        console.log(response);
        setResult(response);
      })
      .catch(error => {
        console.log(error);
      });
   }
  }

  const handleInputChange = (e) => {
    const value = e.target.value;
    const re = /^[0-9,\b]+$/;
    if (e.target.value === '' || re.test(e.target.value))  {
      setError(null);
      setInput(value);
    } else {
      setError("You can only enter a single integer or the integers from 1-n seperated with the comma.");
    }
  }

  return (
    <div className="App">
      <body className="app-body">
        <div className="result-box">
          <code>{JSON.stringify(result)}</code>
        </div>
        <form onSubmit={handleSubmit} className = "form-box">
          <input 
            type="text" 
            onChange={handleInputChange} 
            value={input} 
            className = "input"
            placeholder=" Enter a single integer or the integers from 1-n seperated with the comma."
          />
          {error && (
            <label style={{ color: "red" }} htmlFor="message">
              {error}
            </label>
          )}
          <button className = "form-box" type="submit">Submit</button>
        </form>
      </body>
    </div>
  );
}

export default FormAndResult;