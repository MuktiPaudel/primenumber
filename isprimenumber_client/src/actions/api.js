import axios from 'axios'
const baseUrl = 'https://localhost:44326/api'

const checksumandprime = (numbers) => {
  const request = axios.get(`${baseUrl}/sumandcheck/${numbers}`)
  return request.then(response => response.data)
}

const checkprime = (number) => {
  const action= "checkprime";
  const request = axios.get(`${baseUrl}/?action=${action}&number=${number}`)
  return request.then(response => response.data)
}

export default { checksumandprime, checkprime }