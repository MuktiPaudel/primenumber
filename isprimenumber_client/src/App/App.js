import '../App/App.css';
import FormAndResult from '../components/pages/FormAndResult';
import Header from '../components/layout/Header';


function App() {
  return (
    <div className="App">
         <Header/>
         <FormAndResult />
    </div>
  );
}

export default App;