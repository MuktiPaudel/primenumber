﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IsPrimeNumber_Test
{
    /// <summary>
    /// Hepler class for comparing the isPrime parameter value from json.
    /// </summary>
    public class ValueHelper
    {
        public int result;  
        public bool isPrime;
    }
}
