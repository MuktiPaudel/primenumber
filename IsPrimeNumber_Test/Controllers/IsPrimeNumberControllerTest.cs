using IsPrimeNumer_API.Controllers;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Xunit;

namespace IsPrimeNumber_Test
{
    public class IsPrimeNumberControllerTest
    {
        private IsPrimeNumberController _unitTesting = null;
        public IsPrimeNumberControllerTest()
        {
            if (_unitTesting == null)
            {
                _unitTesting = new IsPrimeNumberController();
            }
        }

        [Theory]
        [InlineData(5, true, true)]
        [InlineData(-5, true, false)]
        [InlineData(1, false, true)]
        [InlineData(0, true, false)]
        [InlineData(2, true, true)]
        [InlineData(89, true, true)]
        public void ValidatePrime(int number, bool expectedResult, bool successExpected = true)
        {
            /* act  */
            IActionResult actualResponse = _unitTesting.CheckPrime(number);

            /* Assert  */
            Assert.NotNull(actualResponse);
            Assert.True((actualResponse is ObjectResult));
            if (successExpected)
            {
                Assert.True(actualResponse is OkObjectResult);
                string jsonStr = JsonConvert.SerializeObject(((ObjectResult)actualResponse)?.Value);
                ValueHelper jsonresult = JsonConvert.DeserializeObject<ValueHelper>(jsonStr);
                Assert.True(expectedResult.Equals(jsonresult.isPrime));
            }
        }

        [Theory]
        [InlineData(new int[] { 5, 1, 1 }, 7, true, true)]
        [InlineData(new int[] { 6, -1, 9 }, 14, false, true)]
        [InlineData(new int[] { 6, 3, -9 }, 0, false, true)]
        [InlineData(new int[] { 0, -1, 4 }, 2, true, false)] /* wrong sum */
        [InlineData(new int[] { 3, 1, 4 }, 8, true, false)]  /* wrong IsprimeStatus */
        public void ValidatePrimeArray(int[] numbers, int expectedSum, bool expectedIsPrimeStatus, bool successExpected = true)  
        {
            /* act  */
            IActionResult actualResponse = _unitTesting.SumAndCheck(numbers);

            /* Assert */
            Assert.NotNull(actualResponse);

            Assert.True((actualResponse is ObjectResult));
            if (successExpected)
            {
                Assert.True(actualResponse is OkObjectResult);
                string jsonStr = JsonConvert.SerializeObject(((ObjectResult)actualResponse)?.Value);
                ValueHelper jsonresult = JsonConvert.DeserializeObject<ValueHelper>(jsonStr);
                Assert.True(expectedSum.Equals(jsonresult.result));
                Assert.True(expectedIsPrimeStatus.Equals(jsonresult.isPrime));
            }
        }
    }
}
