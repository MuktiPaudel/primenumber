﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Threading.Tasks;

public class ArraySupportingRouteValueProviderFactory : IValueProviderFactory
{
    public Task CreateValueProviderAsync(ValueProviderFactoryContext context)
    {
        if (context is null)
        {
            throw new ArgumentNullException(nameof(context));
        }

        var valueProvider = new ArraySupportingRouteValueProvider(
            BindingSource.Path,
            context.ActionContext.RouteData.Values,
            context.ActionContext.ActionDescriptor);

        context.ValueProviders.Add(valueProvider);

        return Task.CompletedTask;
    }
}