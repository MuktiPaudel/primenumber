﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace IsPrimeNumer_API.Controllers
{
    public class BaseController : Controller
    {
        public bool IsPrime(int number) 
        {
            if (number == 1) return false;
            if (number == 2) return true;
            if (number <= 0) return false;

            var limit = Math.Ceiling(Math.Sqrt(number));

            for (int i = 2; i <= limit; ++i)
                if (number % i == 0)
                    return false;
            return true;
        }
    }
}