﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Linq;

namespace IsPrimeNumer_API.Controllers
{
    public class IsPrimeNumberController : BaseController 
    {
        /// <summary>
        /// Passes parameter using FromQuery
        /// URL: https://localhost:44387/api/sumandcheck/1,2,2
        /// </summary>

        [HttpGet("api/sumandcheck/{numbers}")]  
        public IActionResult SumAndCheck(int[] numbers)
        {
            if (numbers != null && numbers.Length != 0)
            {
                int sum = numbers.Sum();
                bool isPrime = this.IsPrime(sum);
                return Ok(new
                {
                    result = sum,
                    isPrime = isPrime
                });
            }
            else
            {
                return StatusCode(StatusCodes.Status404NotFound, new { message = "Invalid input" });
            }
        }

        /// <summary>
        /// Passes parameter using FromQuery
        /// URL: https://localhost:44387/api/?action=checkprime&number=89 
        /// </summary>

        [HttpGet("/api/")]
        public IActionResult CheckPrime(int number)
        {
            if (number > 0)
            {
                bool isPrime = this.IsPrime(number);
                return Ok(new
                {
                    isPrime = isPrime
                });
            }
            else
            {
                return StatusCode(StatusCodes.Status404NotFound, new { message = "Invalid input" });
            }
        }
    }
}

